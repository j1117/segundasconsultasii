<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    public function actionAbout()
    {
        return $this->render('about');
    } 
   
    public function actionConsultaactiverecord01(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Ciclista::find()
                        ->select("COUNT(*)Número_ciclistas")
                        ->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_ciclistas FROM ciclista",
            
        ]);
    }
    
    public function actionConsulta01(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_ciclistas
                    FROM ciclista"
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_ciclistas FROM ciclista",
            
            ]);
    }

    
    public function actionConsultaactiverecord02(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Ciclista::find()
                        ->select("COUNT(*)Ciclistas_banesto")
                        ->where("nomequipo = 'Banesto'")
                        ->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Ciclistas_banesto'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Ciclistas_banesto FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta02(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT COUNT(*)Ciclistas_banesto 
                    FROM ciclista 
                    WHERE nomequipo = 'Banesto'",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Ciclistas_banesto'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Ciclistas_banesto FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
 
    
    public function actionConsultaactiverecord03(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Ciclista::find()
                        ->select("AVG(edad)Edad_media_ciclistas")
                        ->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Edad_media_ciclistas'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            
            "sql"=>"SELECT DISTINCT AVG(edad)Edad_media_ciclistas FROM ciclista",
        ]);
    }
    
    public function actionConsulta03(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT AVG(edad)Edad_media_ciclistas 
                    FROM ciclista",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Edad_media_ciclistas'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            
            "sql"=>"SELECT DISTINCT AVG(edad)Edad_media_ciclistas FROM ciclista",
        ]);
    }

    
    public function actionConsultaactiverecord04(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Ciclista::find()
                        ->select("AVG(edad)Edad_media_Banesto")
                        ->where("nomequipo = 'Banesto'")
                        ->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Edad_media_Banesto'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            
            "sql"=>"SELECT DISTINCT AVG(edad)Edad_media_Banesto FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta04(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT AVG(edad)Edad_media_Banesto 
                    FROM ciclista 
                    WHERE nomequipo = 'Banesto'",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Edad_media_Banesto'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            
            "sql"=>"SELECT DISTINCT AVG(edad)Edad_media_Banesto FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }

    
    public function actionConsultaactiverecord05(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                        ->select("AVG(edad)Edad_media_Equipos")
                        ->groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Edad_media_Equipos'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            
            "sql"=>"SELECT AVG(edad)Edad_media_Equipos FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta05(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT AVG(edad)Edad_media_Equipos 
                    FROM ciclista 
                    GROUP BY nomequipo",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Edad_media_Equipos'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            
            "sql"=>"SELECT AVG(edad)Edad_media_Equipos FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
     public function actionConsultaactiverecord6(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                        ->select("COUNT(*)Ciclistas_por_equipo")
                        ->groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Ciclistas_por_equipo'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            
            "sql"=>"SELECT COUNT(*)Ciclistas_por_equipo FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta06(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT COUNT(*)Ciclistas_por_equipo 
                    FROM ciclista 
                    GROUP BY nomequipo",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Ciclistas_por_equipo'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            
            "sql"=>"SELECT COUNT(*)Ciclistas_por_equipo FROM ciclista GROUP BY nomequipo",
        ]);
    }
        
    public function actionConsultaactiverecord07(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Puerto::find()
                        ->select("COUNT(*)Número_puertos")
                        ->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_puertos'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_puertos FROM puerto",
        ]);
    }
    
    public function actionConsulta07(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_puertos 
                    FROM puerto",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_puertos FROM puerto",
        ]);
    }
    
    public function actionConsultaactiverecord08(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Puerto::find()
                        ->select("COUNT(*)Número_puertos_mayor_de_1500")
                        ->where("altura > 1500")->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_puertos_mayor_de_1500'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_puertos_mayor_de_1500 FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsulta08(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_puertos_mayor_de_1500 
                    FROM puerto 
                    WHERE altura > 1500",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_puertos_mayor_de_1500'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            
            "sql"=>"SELECT DISTINCT COUNT(*)Número_puertos_mayor_de_1500 FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsultaactiverecord09(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Ciclista::find()
                        ->select("nomequipo")
                        ->groupBy("nomequipo")
                        ->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            
            "sql"=>"SELECT DISTINCT nomequipo FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta09(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT nomequipo 
                    FROM ciclista GROUP BY nomequipo",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            
            "sql"=>"SELECT DISTINCT nomequipo FROM ciclista GROUP BY nomequipo",
        ]);
    } 
    
    public function actionConsultaactiverecord10(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=> Ciclista::find()
                        ->select("nomequipo")
                        ->where("edad BETWEEN 28 AND 32")
                        ->groupBy("nomequipo")
                        ->distinct(),      
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            
            "sql"=>"SELECT DISTINCT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta10(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT nomequipo FROM ciclista 
                    WHERE edad BETWEEN 28 AND 32 
                    GROUP BY nomequipo",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            
            "sql"=>"SELECT DISTINCT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsultaactiverecord11(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=> Etapa::find()
                        ->select("COUNT(*)Número_etapas")
                        ->groupBy("dorsal"),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_etapas'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            
            "sql"=>"SELECT COUNT(*)Número_etapas FROM etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta11(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT COUNT(*)Número_etapas FROM etapa GROUP BY dorsal",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['Número_etapas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT COUNT(*)Número_etapas FROM etapa GROUP BY dorsal",
            
        ]);
    }
    
    public function actionConsultaactiverecord12(){
        
        $dataProvider = new ActiveDataProvider([
            
            'query'=> Etapa::find()
                        ->select("dorsal")
                        ->groupBy("dorsal")
                        ->distinct(),
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            
            "sql"=>"SELECT DISTINCT dorsal FROM etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta12(){
        
        $dataProvider = new SqlDataProvider([
            
            "sql"=>"SELECT DISTINCT dorsal 
                    FROM etapa GROUP BY dorsal",
        ]);
        
        return $this->render("resultado",[
            
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            
            "sql"=>"SELECT DISTINCT dorsal FROM etapa GROUP BY dorsal",
        ]);
    }
}
