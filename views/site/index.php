<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas Boletín2</h1>
    </div>

    <div class="body-content">

        <div class="row">
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Número de ciclistas que hay</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord01'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta01'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Número de ciclistas que hay del equipo Banesto</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord02'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta02'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Edad media de los ciclistas</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord03'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta03'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>La edad media de los del equipo Banesto</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord04'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta04'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>La edad media de los del equipo Banesto</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord05'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta05'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>El número de ciclistas por equipo</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord06'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta06'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>El número total de puertos</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord07'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta07'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>El número total de puertos mayores de 1500</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord08'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta08'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord09'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta09'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord10'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta10'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord11'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta11'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
            <div class="col-sm-4 col-md4">
                <div class="card alturaminima"> 
                    <div class="card-body tarjeta">
                        <h3>Consulta 12</h3>
                        <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>
                        <p>
                            <?= html::a('Active Record', ['site/consultaactiverecord12'],['class' =>'btn btn-primary'])?>
                            <?= html::a('DAO', ['site/consulta12'],['class' =>'btn btn-warning'])?>
                        </p>
                    </div>            
                </div>
            </div>
            
        </div>
    </div>
